import React from 'react';

function Task({data, client, title, time}) {
  return (
    <li className="list-group-item">
      <div className="row no-gutters">
        <div className="col-4 col-lg-1">{data}</div>
        <div className="col-6 col-lg-2">{client}</div>
        <div className="col-12 order-12 col-lg">{title}</div>
        <div className="col-2 col-lg-1 order-lg-12">{time}</div>
      </div>
    </li>
  );
}

export default Task;
