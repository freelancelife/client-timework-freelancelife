import React from 'react';
import Task from './Task';
import fetchApi from '../api';

class Tasks extends React.Component {
  constructor() {
    super();

    this.state = {
      items: []
    }
  }
   
  componentDidMount() {
    this.getData();
  }

  getData() {
    fetchApi('list', {
      model:"tasks",
      id:51,
      select:"id,title,type,parent",
      where:{'parent':6}})
    .then(res => this.setState({items: res}))
    .catch(err => console.log(err));
  }

  render() {

    return (
      <ul className="list-group">
        {
          this.state.items
          .map(task => <Task title={task.title}/> )
        }
      </ul>
    );
  }
}

export default Tasks;
