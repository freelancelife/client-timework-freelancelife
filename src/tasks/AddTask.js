import React from 'react';

class AddTask {
  render() {
    return (
    <div className="input-group">
      <input type="text" className="form-control" placeholder="Описание задачи">
      <select className="custom-select w-10" id="inputGroupSelect02">
        <option selected>Клиент...</option>
        <option value="1">motoshar.ru</option>
        <option value="2">1C Снежинск</option>
        <option value="3">Компас</option>
      </select>
      <div className="input-group-append">
        <button className="btn btn-outline-secondary" type="button">Добавить</button>
        <button className="btn btn-outline-secondary" type="button">Отменить</button>
      </div>
    </div>
    );
  }
}

export default AddTask;
