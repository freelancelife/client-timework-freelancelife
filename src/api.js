function fetchApi(cmd, data) {
  const url = 'http://localhost:5000/';
  const sig = '11111111';
  return fetch(`${url}?cmd=${cmd}&data=${JSON.stringify(data)}&sig=${sig}`)
  .then(res => res.json())
  .then(res => {
    if(res.status === 'error') {
      throw Error('Bad response: error code = '+res.error);
    }

    return res.response;
  })
}

export default fetchApi;
